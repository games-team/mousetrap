/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  enemy.cpp -  The 'red baddies'.
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */

#include "mousetrap.h"
#include "enemy.h"

#include <math.h>
#include <stdlib.h>
#include <assert.h>


/**
 * Constructor.
 *
 * Choose random initial starting point, speed and direction.
 */
CEnemy::CEnemy()
{
    /* Random speed */
    speed  = (int)(2 * (rand()/(RAND_MAX+1.0)) + 2);

    /* Random starting position. 
     *
     * NOTE: See note in header for why X is constrained by the
     *       screen height - not it's width ..
     */
    m_x = (int)(SCREEN_HEIGHT * (rand()/(RAND_MAX+1.0)));
    m_y = (int)(SCREEN_HEIGHT * (rand()/(RAND_MAX+1.0)));
    

    /* Random direction of movement */
    int dir  = (int)(8 * (rand()/(RAND_MAX+1.0)));
    
    if ( dir == 0 )
	m_direction = LEFT ; 
    if ( dir == 1 ) 
	m_direction = RIGHT ;
    if ( dir == 2 ) 
	m_direction = UP ; 
    if ( dir == 3 )
	m_direction = DOWN ;
    if ( dir == 4 )
	m_direction = LEFTUP ;
    if ( dir == 5 )
	m_direction = LEFTDOWN ;
    if ( dir == 6 )
	m_direction = RIGHTUP ;
    if ( dir == 7 )
	m_direction = RIGHTDOWN ;	
};



/**
 * Return the current X coordinate.
 */
int CEnemy::get_x()
{ 
    return( m_x ); 
};


/**
 * Return the current Y coordinate.
 */
int CEnemy::get_y()
{ 
    return( m_y ); 
}


/**
 * Kill this object.
 * (This is called when a player possessing the shield powerup
 * bumps into this enemy object).
 */
void CEnemy::kill()
{
    m_x         = -100;
    m_y         = -100;
    m_direction = DEAD;
}


/**
 * Test to see if this enemy object is dead.
 * @return 1 if the object is dead, or 0 otherwise.
 * @seealso kill
 */
int CEnemy::is_dead()
{
    if ( m_direction == DEAD )
	return 1;
    else
	return 0;
}


/**
 * Speedup this object.
 */
void CEnemy::speedup()
{
    speed += 1;
}


/**
 * Perform a single movement step.
 * Pay attention to the screen's bounding box, and
 * change direction as required.
 */
void CEnemy::step()
{
    switch( m_direction )
	{
	case LEFT:
	    m_x -=speed ;
	    if ( m_x < 0 ) 
	    {
		m_x = 0;
		m_direction = RIGHT;
	    }
	    break;
	case RIGHT:
	    m_x += speed;
	    if ( m_x > SCREEN_HEIGHT )
	    {
		m_x = SCREEN_HEIGHT;
		m_direction = LEFT;
	    }
	    break;
	case DOWN:
	    m_y +=speed ;
	    if ( m_y > SCREEN_HEIGHT ) 
	    {
		m_y = SCREEN_HEIGHT;
		m_direction = UP;
	    }
	    break;
	case UP:
	    m_y -=speed ;
	    if ( m_y < 0 )
	    {
		m_y = 0;
		m_direction = DOWN;
	    }
	    break;
	case LEFTUP:
	    m_x -=speed-1 ;
	    if ( m_x < 0 ) 
	    {
		m_x = 0;
		m_direction = RIGHTUP;
	    }
	    m_y +=speed-1 ;
	    if ( m_y > SCREEN_HEIGHT ) 
	    {
		m_y = SCREEN_HEIGHT;
		m_direction = LEFTDOWN;
	    }
	    break;
	case LEFTDOWN:
	    m_x -=speed-1 ;
	    if ( m_x < 0 ) 
	    {
		m_x = 0;
		m_direction = RIGHTDOWN;
	    }
	    m_y -=speed-1 ;
	    if ( m_y < 0 )
	    {
		m_y = 0;
		m_direction = LEFTUP;
	    }
	    break;
	case RIGHTUP:
	    m_x += speed-1;
	    if ( m_x > SCREEN_HEIGHT )
	    {
		m_x = SCREEN_HEIGHT;
		m_direction = LEFTUP;
	    }
	    m_y +=speed-1 ;
	    if ( m_y > SCREEN_HEIGHT ) 
	    {
		m_y = SCREEN_HEIGHT;
		m_direction = RIGHTDOWN;
	    }
	    break;
	case RIGHTDOWN:
	    m_x += speed-1;
	    if ( m_x > SCREEN_HEIGHT )
	    {
		m_x = SCREEN_HEIGHT;
		m_direction = LEFTDOWN;
	    }
	    m_y -=speed-1 ;
	    if ( m_y < 0 )
	    {
		m_y = 0;
		m_direction = RIGHTUP;
	    }
	    break;
	case DEAD:
	    break;
	default:
	    assert( false );
	    break;
	}
}


