/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  enemy.h -  The 'red baddies'.
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */



#ifndef _ENEMY_H
#define _ENEMY_H 1


/**
 *  Enemy class.
 *
 *  Each "enemy" on the screen is represented by an instance of this
 * object.
 *
 * @author Steve Kemp
 *
 */
class CEnemy
{

public:


    /**
     * Constructor.
     *
     * Choose random initial starting point, speed, and direction.
     *
     */
    CEnemy();


    /**
     * Return the current X coordinate.
     * @return The X coordinate of this enemy object
     */
    int get_x();


    /**
     * Return the current Y coordinate.
     * @return The Y coordinate of this enemy object
     */
    int get_y();


    /**
     * Kill this object.
     * (This is called when a player possessing the shield powerup
     * bumps into this enemy object).
     */
    void kill();

    
    /**
     * Test to see if this enemy object is dead.
     * @return 1 if the object is dead, or 0 otherwise.
     * @seealso kill
     */
    int is_dead();


    /**
     * Speedup this object.
     */
    void speedup();


    /**
     * Perform a single movement step.
     * Pay attention to the screen's bounding box, and
     * change direction as required.
     */
    void step();


private:

    /**
     * Directions the baddies move in - this is private to
     * the class.
     */
    typedef enum { LEFT, RIGHT, UP, DOWN, 
		   LEFTUP, LEFTDOWN, RIGHTUP, RIGHTDOWN,
		   DEAD
    } tDirections;
    

    /**
     * Current X coordinate.
     */
    int m_x;
    
    /**
     * Current Y coordiante.
     */
    int m_y;
    
    /**
     * Current direction.
     */
    tDirections m_direction;

    /**
     * Current speed.
     */
    int speed;
    
};


#endif  /* _ENEMY_H */
