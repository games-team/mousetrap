/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  highscore.h -  A singleton for holding the game's highscore
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */

#ifndef _HIGHSCORE_H
#define _HIGHSCORE_H 1


/**
 * A utility class for dealing with highscores.
 *
 * This class makes use of the Singleton Design Pattern - as there
 * only ever one highscore.
 *
 * It also abstracts away the reading and writing of highscore
 * entries.
 *
 * @author Steve Kemp
 *
 */
class CHighScore
{

public:


    /**
     * Gain access to the one and only instance of the highscore object.
     * @returns The Highscore Singleton.
     */
    static CHighScore *getInstance();


    /**
     * Read and return the current highscore.
     * @return The current highscore.
     */
    int readHighScore();


    /**
     * Save away the best score.
     */
    void saveHighScore( int score );


protected:


    /**
     * Constructor - this is protected as this class uses
     * the singleton design pattern.
     */
    CHighScore();


private:


    /**
     * The one and only CHighScore object.
     */
    static CHighScore *m_instance;


    /**
     * Cache marker.
     */
    int m_valid;


    /**
     * Highscore cache.
     */
    int m_highscore;
};


#endif /* _HIGHSCORE_H */
