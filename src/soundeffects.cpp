/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  soundeffects.cpp -  A singleton for holding sound effects
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include "soundeffects.h"


/*
 * Set our instance pointer to NULL - this will ensure the
 * singleton accessor works as expected.
 */
CSoundEffects *CSoundEffects::m_instance = NULL;



/**
 * Constructor.
 *
 * This isn't public as we're a singleton.
 */
CSoundEffects::CSoundEffects()
{
    /*
     * Assume we loaded OK.
     */
    m_enabled = 1;

    /*
     * Setup the audio
     */
    Mix_OpenAudio(44100, AUDIO_S16, 2, 2048);

    /*
     * Load all our samples - this will disable this
     * object if any of the samples fails to load.
     */
    setupSamples();


}


/**
 * Gain access to the one and only instance of this object.
 * @returns The Sound Effects Singleton.
 */
/* static */ CSoundEffects *CSoundEffects::getInstance()
{
    if ( !m_instance )
    {
	m_instance = new CSoundEffects();
    }

    return( m_instance );
}

/**
 * Setup all our samples.
 */
void CSoundEffects::setupSamples()
{

    m_eat_sound      = loadSound( (char *)"data/audio/sounds_eat.wav" );
    m_lastlife_sound = loadSound( (char *)"data/audio/sounds_lastlife.wav" );
    m_oneup_sound    = loadSound( (char *)"data/audio/sounds_oneup.wav" );
    m_laser_sound    = loadSound( (char *)"data/audio/sounds_laser.wav" );

    if ( ( m_eat_sound      == NULL ) ||
	 ( m_lastlife_sound == NULL ) ||
	 ( m_oneup_sound    == NULL ) ||
	 ( m_laser_sound    == NULL ) )
	m_enabled = 0;

}



/**
 *  Load an image.
 *
 * Either from:
 *
 *  ./
 *
 * or
 *
 * /usr/share/mousetrap/
 *
 * Failure to load just disables sound, it is non-fatal.
 */
Mix_Chunk *CSoundEffects::loadSound(char * path )
{
    char f_path[1024];

    int i;
    i = access ( path, F_OK );

    if ( i == 0 )
    {
	return( Mix_LoadWAV(path) );
    }
    else
    {
	snprintf(f_path, sizeof(f_path), "/usr/share/mousetrap/%s",path);
	i = access ( f_path, F_OK );

	if ( i == 0 )
	{
	    return( Mix_LoadWAV(f_path) );
	}
    }


    return( NULL );
}


/**
 * Play a sound.
 */
void CSoundEffects::playSound (Mix_Chunk * thesound)
{
    Mix_PlayChannel(-1, thesound, 0);
}


/**
 * Play the 'last life' sound.
 */
void CSoundEffects::lastLife()
{
    if ( m_enabled )
	playSound( m_lastlife_sound );
}

/**
 * Play the 'one up' sound.
 */
void CSoundEffects::oneUp()
{
    if ( m_enabled )
	playSound( m_oneup_sound );
}


/**
 * Play the 'eat' sound.
 */
void CSoundEffects::eat()
{
    if ( m_enabled )
	playSound( m_eat_sound );
}


/**
 * Play the 'laser' sound.
 */
void CSoundEffects::laser()
{
    if ( m_enabled )
	playSound( m_laser_sound );
}

