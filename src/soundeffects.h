/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  soundeffects.h -  A singleton for holding sound effects
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */



#ifndef _SOUNDEFFECTS_H
#define _SOUNDEFFECTS_H 1

#include <SDL.h>
#include <SDL_mixer.h>           /* Sound playing */



/**
 *  A utility class for producing the limited set of sound effects
 * the game supports.
 *
 * This class makes use of the Singleton Design Pattern - as there
 * only ever one collection of sound effects in use.
 *
 *
 * @author Steve Kemp
 *
 */
class CSoundEffects
{

public:
    /**
     * Gain access to the one and only instance of the highscore object.
     * @returns The Highscore Singleton.
     */
    static CSoundEffects *getInstance();


public:


    /**
     * Play the 'eat' sound.
     */
    void eat();


    /**
     * Play the 'one up' sound.
     */
    void oneUp();


    /**
     * Play the 'last life' sound.
     */
    void lastLife();


    /**
     * Play the 'laser' sound.
     */
    void laser();


protected:


    /**
     * Constructor - this is protected as this class uses
     * the singleton design pattern.
     */
    CSoundEffects();


    /**
     * Load up the different samples.
     * Protected as this is called only in our constructor.
     * @seealso loadSound
     */
    void setupSamples();


    /**
     * Load a single sound sample.
     */
    Mix_Chunk *loadSound(char * path );


    /**
     * Play a single sound sample.
     */
    void playSound(Mix_Chunk * thesound);


private:


    /**
     * The one and only CSoundEffects object.
     */
    static CSoundEffects *m_instance;


    /**
     * Are we enabled?  We will assume we are unless loading a sample
     * fails.
     * @seealso setupSamples
     */
    int m_enabled;

private:

    /* Sounds we play. */
    Mix_Chunk * m_eat_sound;
    Mix_Chunk * m_lastlife_sound;
    Mix_Chunk * m_oneup_sound;
    Mix_Chunk * m_laser_sound;
};


#endif /* _SOUNDEFFECTS_H */
